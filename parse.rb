#!/usr/bin/env ruby

require "curb"
require "nokogiri"
require "csv"

def parse address, output_filename
    root_page = get_page address

    puts "calculate page count"
    heading_counter = root_page.xpath(".//*/span[@class='heading-counter']").text
    category_products_count = Integer(/\d+/.match(heading_counter)[0])

    root_page_products_links = get_products_links root_page
    page_products_count = root_page_products_links.length
    page_count = (category_products_count/page_products_count.to_f).ceil

    puts "category product count #{category_products_count}"
    puts "product per page #{page_products_count}"
    puts "page count: #{page_count}"

    begin
        csv_file = CSV.open(output_filename, "wb")
    rescue
        puts "FAIL WHILE OPEN OUTPUT FILE"
        exit
    end

    begin
        root_page_products_links.each do |product_link|
            parse_products product_link, csv_file
            puts
        end

        for i in 2..page_count
            puts "Page №#{i}"
            page_link = "#{address}?p=#{i}"
            page = get_page page_link
            puts
            products = get_products_links page
            products.each do |product_link|
                parse_products product_link, csv_file
                puts ""
            end
        end
    rescue Exception => e
        puts "ERROR: #{e.message}" 
        exit
    ensure
        csv_file.close
    end
    puts "SUCCESS PARSE"
    puts "result file #{output_filename}"
end

def get_page address
    puts "send request to #{address}"
    http_request = Curl.get(address)
    http_content = http_request.body_str
    html = Nokogiri::HTML(http_content)

    html
end

def get_products_links page
    products = page.xpath(".//*/div[@class='product-container']/*/div/a[@class='product_img_link product-list-category-img']")
    products_page_links = []
    products.each do |link|
        products_page_links += [link["href"]]
    end
    return products_page_links
end

def parse_products page_link, csv_file
    page = get_page page_link
    product_global_name = page.xpath(".//h1[@class='product_main_name']").text.strip
    puts "product: #{product_global_name}"

    puts "search images"
    pictures_html = page.xpath("//*[@id='thumbs_list']//img")
    pictures = []
    pictures_html.each do |picture|
        pictures += [picture["src"]]
    end
    puts "found #{pictures.length} images"
    picture_index = 0

    puts "search subproducts"
    subproducts = page.xpath(".//*/div[@class='attribute_list']/ul[@class='attribute_radio_list']/li/label")
    if subproducts.length == 1
        puts "parse single product page"
    else
        puts "parse multiproduct page"
        puts "found #{subproducts.length} subproducts"
    end
    subproducts.each do |product|
        subproduct_name = product.xpath(".//span[@class='radio_label']").text
        subproduct_price_text = product.xpath(".//span[@class='price_comb']").text
        subproduct_price = /\d+.\d+/.match(subproduct_price_text)[0]
        csv_file << ["#{product_global_name.strip} - #{subproduct_name.strip}", subproduct_price, pictures[picture_index]]
        picture_index = (picture_index + 1) % pictures.length
    end
end

def get_address
    if ARGV.length < 1
        raise "need input address"
    end

    address = ARGV[0]

    if address[-1] != "/"
        address += "/"
    end

    address
end

def get_output
    if ARGV.length < 2
        return "output.csv"
    end

    output_path = ARGV[1]

    output_path
end

if __FILE__ == $0
    begin
        address = get_address
        output_file = get_output
    rescue Exception => e
        puts "ERROR: #{e.message}"
        exit
    end

    # "https://www.petsonic.com/snacks-huesos-para-perros/", "output.csv"
    parse address, output_file
end